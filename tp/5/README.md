# TP5 : Deploy the world

Dernier TP consacré à la remise en place de toutes les technos vues en cours précédemment.

Le sujet de ce TP est simple : **monter l'application de votre choix**, avec uniquement du **déploiement automatisé**, en partant de rien jusqu'à l'application qui tourne.

➜ **J'attends au minimum :**

- **un plan Terraform**
  - il crée la ou les machine(s) nécessaire(s) au bon fonctionnement de votre solution
- **un playbook Ansible**
  - il déploie de la conf système
  - il déploie l'application choisie

**Vous pouvez ajouter :**

- **du cloud-init**
  - pour autoconfigurer la machine à son premier boot
  - pouvoir utiliser tout de suite Ansible une fois que la VM a démarré
- **de la conteneurisation**
  - `Dockerfile` + `docker-compose.yml`
  - ça reste dans notre contexte de cours : des fichiers texte qui déploie de l'infra !
- **de la CI/CD**
  - code ansible testé puis déployé automatiquement sur la VM
  - build d'une image automatique, push dans un registre, puis déploiement sur la VM

➜ Le but n'est pas juste d'accumuler techno sur techno juste pour le plaisir, mais de servir un vrai but.

L'ensemble des fichiers que vous allez produire va constituer quelque chose de complètement autonome, que l'on peut déployer à volonté.

> Si c'est vraiment le vide dans votre esprit pour trouver un truc à fournir comme service, je donne ce lien souvent en ce moment, qui répertorie une liste assez grande de techno libres et open-source pour héberger tel ou tel type de service : https://github.com/awesome-selfhosted/awesome-selfhosted

➜ **Créez un dépôt git dédié à ce TP5**

Ajoutez simplement un `README.md` dans votre dépôt principal pour le rendu du TP5, qui contient juste l'URL vers le dépôt que vous avez créé spécifiquement pour le TP5.
