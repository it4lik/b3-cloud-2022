# Cloud B3 2022

Vous trouverez ici les cours et TPs abordés lors des séances Cloud.

### TPs

- [TP0 : Vagrant & Ansible chill](./tp/0/README.md)
- [TP1 : Ansible](./tp/1/README.md)
- [TP2 : First boot conf](./tp/2/README.md)
- [TP3 : Terraform](./tp/3/README.md)
- [TP4 : CI/CD](./tp/4/README.md)
- [TP5 : Deploy the world](./tp/5/README.md)

![Un potit schéma](./cours/pics/cloud_infra.png)
